const { review, movie } = require("../models");

class ReviewController {
  async create(req, res) {
    try {
      req.body.user = req.user.id;
      console.log(req.body);
      // Create data
      let data = await review.create(req.body);
      console.log(req.body);

      return res.status(201).json({
        message: "Success",
        data,
      });
    } catch (e) {
      if (
        e.code == 11000 &&
        e.keyPattern.movie_id == 1 &&
        e.keyPattern.user_id == 1
      ) {
        //console.log(e);
        return res.status(400).json({
          message: "Error",
          error: "User has been reviewed this movie",
        });
      } else {
        console.log(e);
        return res.status(500).json({
          message: "Internal Server Error",
          error: e.message,
        });
      }
    }
  }

  async getAllreviewByUser(req, res) {
    try {
      req.body.user = req.user.id;
      // Find all data
      const datareviews = await review
        .find({ user: req.body.user })
        .populate("movie");
      // If no data
      if (datareviews.length === 0) {
        return res.status(404).json({
          message: "No reviews found",
        });
      }
      // If successful
      return res.status(200).json({
        message: "Success",
        count: datareviews.length,
        data: datareviews,
      });
    } catch (e) {
      console.log(e);
      return res.status(500).json({
        message: "Internal Server Error",
        error: e.message,
      });
    }
  }

  async update(req, res) {
    try {
      req.body.userId = req.user.id;
      let data = await review.findOneAndUpdate(
        {
          _id: req.params.id,
        },
        req.body, // This is all of req.body
        {
          new: true,
        }
      );
      // If success
      return res.status(201).json({
        message: "Success",
        data,
      });
    } catch (e) {
      if (
        e.code == 11000 &&
        e.keyPattern.movie_id == 1 &&
        e.keyPattern.user_id == 1
      ) {
        console.log(e);
        return res.status(400).json({
          message: "Error",
          error: "User has been reviewed this movie",
        });
      } else {
        console.log(e);
        return res.status(500).json({
          message: "Internal Server Error",
          error: e.message,
        });
      }
    }
  }

  async delete(req, res) {
    try {
      await review.delete();

      // If success
      return res.status(200).json({
        message: "Success to delete review",
      });
    } catch (e) {
      // If failed
      return res.status(500).json({
        message: "Internal Server Error",
        error: e.message,
      });
    }
  }
}

module.exports = new ReviewController();
