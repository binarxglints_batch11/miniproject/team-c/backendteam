const validator = require("validator");
const mongoose = require("mongoose");
const { user, review, movie, cast } = require("../../models");

exports.create = async (req, res, next) => {
    try {
        let errors = [];

        // Check id_movie is valid or not
        if (!mongoose.Types.ObjectId.isValid(req.body.movie)) {
            errors.push(
                "movie_id is not valid and must be 24 character & hexadecimal"
            );
        }

        // Check movie_id is valid or not
        // If the parameters is not valid it will go here
        if (errors.length > 0) {
            return res.status(400).json({
                message: errors.join(", "),
            });
        }

        // Find movie andreview
        let findData = await Promise.all([
            movie.findOne({ _id: req.body.movie }),
        ]);

        // if movie not found
        if (!findData[0]) {
            errors.push("Movie not found");
        }

        // Check is rating numeric?
        if (!validator.isNumeric(req.body.rating)) {
            errors.push("Rating must be a number");
        } else {
          if (req.body.rating > 10 || req.body.rating < 1 ) {
            errors.push("Rating must be a number 1 to 5");
          }
        }

        // If errors
        if (errors.length > 0) {
            return res.status(400).json({
                message: errors.join(", "),
            });
        }

        // Go to next
        next();
    } catch (e) {
        return res.status(500).json({
            message: "Internal Server Error",
            error: e.message,
        });
    }
};

exports.update = async (req, res, next) => {
    try {
        let errors = [];

        // Check parameter id is valid or not
        if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
            errors.push(
                "id_review is not valid and must be 24 character & hexadecimal"
            );
        }

        // Check id movie is valid or not
        if (!mongoose.Types.ObjectId.isValid(req.body.movie)) {
            errors.push(
                "movie is not valid and must be 24 character & hexadecimal"
            );
        }

        // If the parameters is not valid it will go here
        if (errors.length > 0) {
            return res.status(400).json({
                message: errors.join(", "),
            });
        }

        // Find movie,review 
        let findData = await Promise.all([
            movie.findOne({ _id: req.body.movie }),
            review.findOne({ _id: req.params.id }),
        ]);

        // if movie not found
        if (!findData[0]) {
            errors.push("movie not found");
        }

        // If review not found
        if (!findData[1]) {
            errors.push("review not found");
        }


        // Check rating is numeric
        if (!validator.isNumeric(req.body.rating)) {
            errors.push("Rating must be a number");
        } else {
          if (req.body.rating > 5 || req.body.rating < 1 ) {
            errors.push("Rating must be a number 1 to 5");
          }
        }

        if (errors.length > 0) {
            return res.status(400).json({
                message: errors.join(", "),
            });
        }

        // Go to next
        next();
    } catch (e) {
        return res.status(500).json({
            message: "Internal Server Error",
            error: e.message,
        });
    }
}

exports.delete = async (req, res, next) => {
    try {
        let errors = [];

        // Check params is valid or not
        if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
            errors.push(
                "id_review is not valid and must be 24 character & hexadecimal"
            );
        }

        // If params error
        if (errors.length > 0) {
            return res.status(400).json({
                message: errors.join(", "),
            });
        }

        // Find one Review
        let data = await review.findOne({ _id: req.params.id });

        // If Review not found
        if (!data) {
            errors.push("Review not found");
        }

        // If error
        if (errors.length > 0) {
            return res.status(400).json({
                message: errors.join(", "),
            });
        }

        // Go to next
        next();
    } catch (e) {
        return res.status(500).json({
            message: "Internal Server Error",
            error: e.message,
        });
    }
};
